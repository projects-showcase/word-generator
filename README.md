<h1> Word Generator </h1>

A tool to help writers who are struggling to generate ideas. The website prompts a user with a word that the user must then use to form the next sentence of a story. It's pretty fun.